﻿/** Various stuff for working with _templates.

Copyright: Denis Shelomovskij 2011-2012

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstd.templates;


import unstd.generictuple;
import unstd.traits;


/**
Instantiate a template $(D Template) using arguments $(D A).
*/
alias Inst(alias Template, A...) = Template!A;

///
unittest
{
	import std.traits: PointerTarget;

	static assert(is(Inst!(PointerTarget, int*) == int));
}

unittest
{
	import std.traits: isPointer;
	static assert( Inst!(isPointer, int*));
	static assert(!Inst!(isPointer, int ));
	static assert(Inst!(GenericTuple, 5)[0] == 5);
}


/**
Create template from a string $(D Pred).
If $(D Pred) isn't a string, alises itself to $(D Pred).

If $(D argumentsCount) is $(D -1) created template will accept any number
of arguments, otherwise it will expect $(D argumentsCount) arguments.

If $(D EnumType) is $(D void) created template may be an $(D alias) or
an $(D enum), otherwise it will be an $(D enum) of type $(D EnumType).

Created template can access its aruments as a generic tuple with $(D Args).

If $(D argumentsCount) is $(D 1) or $(D 2) created template can access
its first argument with $(D a) if it is an value, with $(D T) if it is a type
and with $(D A) otherwise.

If $(D argumentsCount) is $(D 2) created template can access
its second argument with $(D b) if it is an value, with $(D U) if it is a type
and with $(D B) otherwise.

$(D UnaryTemplate) is a convinient way to create a template with one argument ($(D argumentsCount) is $(D 1)).

$(D BinaryTemplate) is a convinient way to create a template with two arguments ($(D argumentsCount) is $(D 2)).
*/
template Template(alias Pred, int argumentsCount, EnumType = void)
	if(argumentsCount >= -1)
{
	static if(isSomeString!(typeof(Pred)))
	{
		template Template(Args...) if(argumentsCount == -1 || Args.length == argumentsCount)
		{
			static if(argumentsCount >= 1 && argumentsCount <= 2)
			{
				static if(__traits(compiles, { enum e = Args[0]; }))
					enum a = Args[0];
				else static if(is(Args[0]))
					alias T = Args[0];
				else
					alias A = Args[0];

				static if(argumentsCount == 2)
				{
					static if(__traits(compiles, { enum e = Args[1]; }))
						enum b = Args[1];
					else static if(is(Args[1]))
						alias U = Args[1];
					else
						alias B = Args[1];
				}
			}

			static if(is(EnumType == void))
			{
				static if(__traits(compiles, { enum e = mixin(Pred); }))
					enum Template = mixin(Pred);
				else
					mixin(`alias Template = `~Pred~`;`);
			}
			else
			{
				enum EnumType Template = mixin(Pred);
			}
		}
	} else
		alias Template = Pred;
}

/// ditto
alias UnaryTemplate(alias Pred, EnumType = void) = Template!(Pred, 1, EnumType);

/// ditto
alias BinaryTemplate(alias Pred, EnumType = void) = Template!(Pred, 2, EnumType);

///
unittest
{
	static assert(Inst!(UnaryTemplate!`__traits(isUnsigned, T)`, uint));
	static assert(is(Inst!(UnaryTemplate!`T[]`, int) == int[]));
	static assert(Inst!(UnaryTemplate!`a == 5`, 5));
	static assert(Inst!(BinaryTemplate!`a == 1 && b == 2`, 1, 2));
	static assert(Inst!(BinaryTemplate!`a + U.sizeof`, 1, int) == 5);
	static assert(PackedGenericTuple!(Inst!(Template!(`Args`, -1), "x", int)).equals!("x", int));
}

unittest
{
	static assert(Inst!(UnaryTemplate!`!__traits(isUnsigned, T)`,  int));
	static assert(Inst!(Inst!(UnaryTemplate!notTemplate, isPointer), int));
	static assert(Inst!(Inst!(UnaryTemplate!`notTemplate!A`, isPointer), int));
	static assert(Inst!(Inst!(UnaryTemplate!(notTemplate!notTemplate), isPointer), int*));
	static assert(Inst!(Inst!(UnaryTemplate!`Inst!(notTemplate!notTemplate, A)`, isPointer), int*));

	static assert(Inst!(UnaryTemplate!`a == 7`w, 7));

	static assert(!__traits(compiles, Inst!(Template!(`T`, bool), int)));

	static assert(PackedGenericTuple!(Inst!(Template!(`Args`, -1), 1, int, "x")).equals!(1, int, "x"));
}


/**
Using $(D unaryPred) or $(D binaryPred) is a convinient way to create a template
with one or two arguments respectively which is an $(D enum) of type $(D bool).

It is equal to instantiating $(MREF Template) with corresponding
$(D argumentsCount) and $(D bool) as $(D EnumType).
*/
alias unaryPred(alias pred) = UnaryTemplate!(pred, bool);

/// ditto
alias binaryPred(alias pred) = BinaryTemplate!(pred, bool);

///
unittest
{
	static assert(Inst!(unaryPred!`__traits(isUnsigned, T)`, uint));
	static assert(Inst!(binaryPred!`a == U.sizeof`, 4, int));
}

unittest
{
	static assert(Inst!(unaryPred!`!__traits(isUnsigned, T)`,  int));
	static assert(Inst!(unaryPred!`a == 5`, 5));
	static assert(!__traits(compiles, Inst!(unaryPred!`T`, int)));
}


/**
Create predicate template returning $(D !template_).
*/
template notTemplate(alias template_)
{
	template notTemplate(T...)
	{
		static if(__traits(compiles, { enum e = template_!T; }))
			enum bool notTemplate = !template_!T;
		else
			template notTemplate(U...)
			{
				enum bool notTemplate = !Inst!(template_!T, U);
			}
	}
}

///
unittest
{
	import std.traits: isPointer;

	alias notPointer = notTemplate!isPointer;
	static assert( notPointer! int );
	static assert(!notPointer!(int*));

	alias toBoolTemplate = notTemplate!notTemplate;
	static assert(Inst!(toBoolTemplate!isPointer, int*));
	template get5() { enum get5 = 5; }
	static assert(Inst!(toBoolTemplate!get5) == true);
}

unittest
{
	alias notPointer = notTemplate!isPointer;
	static assert( notPointer! int );
	static assert(!notPointer!(int*));
	static assert( Inst!(notTemplate!isPointer, int ));
	static assert(!Inst!(notTemplate!isPointer, int*));

	alias toBoolTemplate = notTemplate!notTemplate;

	alias _isPointer = toBoolTemplate!isPointer;
	static assert(!_isPointer! int );
	static assert( _isPointer!(int*));

	alias get5 = Template!("5", 0);
	static assert(Inst!(toBoolTemplate!get5) == true);
}


/**
Create predicate template returning $(D true) iff there are no templates or all
$(D templates) return non-zero.
*/
template andTemplates(templates...)
{
	template andTemplates(T...)
	{
		static if(templates.length == 0)
			enum andTemplates = true;
		else static if(Inst!(templates[0], T))
			enum andTemplates = Inst!(.andTemplates!(templates[1 .. $]), T);
		else
			enum andTemplates = false;
	}
}

///
unittest
{
	import std.traits: isIntegral, isSigned;

	alias isSignedIntegral = andTemplates!(isIntegral, isSigned);
	static assert( allTuple!(isSignedIntegral,  int,  short, long));
	static assert(!anyTuple!(isSignedIntegral, uint, ushort, ulong));

	alias isShort = andTemplates!(isSignedIntegral, unaryPred!`is(T == short)`);
	static assert( isShort!short);
	static assert(!anyTuple!(isShort, int, long, uint, ushort, ulong));
}

unittest
{
	alias _true = andTemplates!();
	static assert(_true!() && _true!int && _true!(int, int*));

	import std.traits: isPointer;

	alias _isPointer = andTemplates!isPointer;
	static assert(_isPointer!(int*) && !_isPointer!int);
}


/**
Create predicate template returning $(D true) iff any template of
$(D templates) return non-zero (i.e. returning $(D false) if there
are no templates).
*/
template orTemplates(templates...)
{
	template orTemplates(T...)
	{
		static if(templates.length == 0)
			enum orTemplates = false;
		else static if(!Inst!(templates[0], T))
			enum orTemplates = Inst!(.orTemplates!(templates[1 .. $]), T);
		else
			enum orTemplates = true;
	}
}

///
unittest
{
	import std.traits: isIntegral, isFloatingPoint;

	alias isIntegralOrFloating = orTemplates!(isIntegral, isFloatingPoint);
	static assert( allTuple!(isIntegralOrFloating, int,  short, long, float, double));
	static assert(!anyTuple!(isIntegralOrFloating, bool, char));

	alias isIntegralOrFloatingOrChar = orTemplates!(isIntegralOrFloating, unaryPred!`is(T == char)`);
	static assert( allTuple!(isIntegralOrFloatingOrChar, int, short, long, float, double, char));
	static assert(!isIntegralOrFloatingOrChar!bool);
}

unittest
{
	alias _false = orTemplates!();
	static assert(!_false!() && !_false!int && !_false!(int, int*));

	import std.traits: isPointer;

	alias _isPointer = orTemplates!isPointer;
	static assert(_isPointer!(int*) && !_isPointer!int);
}


/**
Binds template arguments.

$(UL
	$(LI use $(D args[i]) or $(D arg!i) to refer $(D i)-th argument;)
	$(LI use $(D args[$ - i]) to refer arguments from the end
		(also $(D args[$ + i]) can be used for negative $(D i));)
	$(LI use $(D args[a .. b]) or $(D argsRange!(a, b)) to refer arguments from
		$(D a)-th up to and excluding $(D b)-th;)
	$(LI use $(D argsToEnd!n) to refer arguments from $(D n)-th argument up to
		the end;)
	$(LI use $(D allArgs) to refer all arguments.)
)

Example:
----
import unstd.traits;

static assert(is(Inst!(BindTemplate!(CommonType, long, allArgs), int) == long));
static assert(!Inst!(BindTemplate!(isImplicitlyConvertible, args[0], int), long));
static assert( Inst!(BindTemplate!(isImplicitlyConvertible, int  , arg!0), long));

alias UnqualAll = BindTemplate!(MapTuple, Unqual, allArgs);
static assert(is(UnqualAll!(const(int), immutable(bool[])) == TypeTuple!(int, immutable(bool)[])));
----

Bugs:
Currently there is no support for $(D args[a .. $]) because of compiler limitations.
*/
template BindTemplate(alias Template, BindArgs...)
{
	alias BindTemplate(Args...) =
		Template!(TemplateBindArgs!(BindArgs.length, BindArgs, Args));
}

// Note: unittest can't be used as an example here as there is no way to place it before `Bugs` section.

private struct __BindArgs
{
	struct Arg { size_t n; }
	struct ArgsRange { size_t from, to; }
	struct ArgsToEnd { size_t from; }
	struct ArgDollar
	{
		int sub = 0;
		auto opBinary(string op)(int n) const
			if(op == "+" || op == "-")
		{ return ArgDollar(op == "+" ? -n : n); }
	}

	auto opDollar() const { return ArgDollar(); }
	auto opIndex(size_t n)               const { return Arg(n); }
	auto opIndex(ArgDollar d)            const { return d; }
	auto opSlice(size_t from, size_t to) const { return ArgsRange(from, to); }
	auto opSlice()                       const { return ArgsToEnd(0); }
}

enum args = __BindArgs();
template arg(size_t n) { enum arg = __BindArgs.Arg(n); }
template argsRange(size_t from, size_t to) { enum argsRange = __BindArgs.ArgsRange(from, to); }
template argsToEnd(size_t from) { enum argsToEnd = __BindArgs.ArgsToEnd(from); }
enum allArgs = argsToEnd!0;

private template TemplateBindArgs(size_t bindedCount, T...)
{
	static if(!bindedCount)
	{
		alias TemplateBindArgs = GenericTuple!();
	}
	else
	{
		alias Args = T[bindedCount .. $];
		alias Rest = TemplateBindArgs!(bindedCount-1, T[1..$]);

		static if(is(typeof(T[0]) == __BindArgs.Arg))
		{
			alias TemplateBindArgs = GenericTuple!(Args[T[0].n], Rest);
		}
		else static if(is(typeof(T[0]) == __BindArgs.ArgDollar))
		{
			alias TemplateBindArgs = GenericTuple!(Args[Args.length - T[0].sub], Rest);
		}
		else static if(is(typeof(T[0]) == __BindArgs.ArgsRange))
		{
			alias TemplateBindArgs = GenericTuple!(Args[T[0].from .. T[0].to], Rest);
		}
		else static if(is(typeof(T[0]) == __BindArgs.ArgsToEnd))
		{
			alias TemplateBindArgs = GenericTuple!(Args[T[0].from .. $], Rest);
		}
		else
		{
			alias TemplateBindArgs = GenericTuple!(T[0], Rest);
		}
	}
}

unittest
{
	alias Pack = PackedGenericTuple;
	static assert(Pack!(Inst!(BindTemplate!(GenericTuple, 1, 2, int), 3)).equals!(1, 2, int));
	static assert(Inst!(BindTemplate!(GenericTuple, arg!0), 3) == expressionTuple!3);
	static assert(Pack!(Inst!(BindTemplate!(GenericTuple, 1, 2, int, args[0]), 3)).equals!(1, 2, int, 3));
	static assert(Pack!(Inst!(BindTemplate!(GenericTuple, 1, 2, int, allArgs), 3)).equals!(1, 2, int, 3));
	static assert(Pack!(Inst!(BindTemplate!(GenericTuple,
			1, args[0 .. 1], 2, int, args[$ - 1]
		),
			3
		)).equals!(
			1, 3, 2, int, 3));
	static assert(Pack!(Inst!(BindTemplate!(GenericTuple,
			1, arg!1, 2, arg!0, int, args[$ + -3], allArgs,
		),
			3, char, 5
		)).equals!(
			1, char, 2, 3, int, 3, 3, char, 5));

	import unstd.traits;

	static assert(is(Inst!(BindTemplate!(CommonType, long, allArgs), int) == long));
	static assert(!Inst!(BindTemplate!(isImplicitlyConvertible, args[0], int), long));
	static assert( Inst!(BindTemplate!(isImplicitlyConvertible, int  , arg!0), long));

	alias UnqualAll = BindTemplate!(MapTuple, Unqual, allArgs);
	static assert(is(UnqualAll!(const(int), immutable(bool[])) == TypeTuple!(int, immutable(bool)[])));
}

// allArgs -> %*, arg! -> %
string formatBind(string fmt)
{
	string res, id;

	int state = 0;
	foreach(i, char c; fmt)
	{
		if(state == 0 && c == '=')
		{
			id = fmt[0 .. i];
		}
		else if(state == 0 && c == '!')
		{
			res = fmt[id ? id.length + 1 : 0 .. i];
			res ~= ',';
			state = 1;
		}
		else if(state == 1 && c == '(')
		{
			fmt = fmt[i + 1 .. $];
			state = 2;
			break;
		}
	}
	assert(state == 2, "invalid format string, can't find '!(': '" ~ fmt ~ "'");


	foreach_reverse(i, char c; fmt)
		if(c == ')')
		{
			if(!id)
				id = fmt[i + 1 .. $];
			fmt = fmt[0 .. i];
			state = 3;
			break;
		}
	assert(state == 3, "invalid format string, can't find ')': " ~ fmt ~ "'");


	bool ctrl = false;
	size_t start = 0;
	foreach(i, char c; fmt)
	{
		if(ctrl)
		{
			ctrl = false;
			if(c == '%') // %% -> %
			{ }
			else if(c == '*') // %* -> allArgs
			{
				res ~= "allArgs";
				++start;
			}
			else if(c.isDigit()) // %# -> arg!#
				res ~= "arg!";
		}
		else if(c == '%')
		{
			res ~= fmt[start .. i];
			start = i + 1;
			ctrl = true;
		}
	}
	assert(!ctrl, "'%' at end of format string: '" ~ fmt ~ "'");


	res ~= fmt[start .. $];
	res = id ~ " = BindTemplate!(" ~ res ~ ")";
	return res;
}

unittest
{
	enum beg = "x = BindTemplate!(";
	static assert(formatBind("x=ab!()") == beg ~ "ab,)");
	static assert(formatBind("x = ab !()") == "x  = BindTemplate!( ab ,)");
	static assert(formatBind("ab!()x") == beg ~ "ab,)");
	static assert(formatBind("ab !() x") == " x = BindTemplate!(ab ,)");
	static assert(formatBind("ab ! ()x") == beg ~ "ab ,)");
	static assert(formatBind("t!(ab%%c)x") == beg ~ "t,ab%c)");
	static assert(formatBind("t!(ab%%)x") == beg ~ "t,ab%)");
	static assert(formatBind("t!(ab%0c)x") == beg ~ "t,abarg!0c)");
	static assert(formatBind("t!(ab%10)x") == beg ~ "t,abarg!10)");
	static assert(formatBind("t!(ab%0)x") == beg ~ "t,abarg!0)");
	static assert(formatBind("t!(ab%0c%1d)x") == beg ~ "t,abarg!0carg!1d)");
}

/**
Binds template arguments using format string.

$(UL
	$(LI use $(D %i) to refer $(D i)-th argument;)
	$(LI use $(D %*) to refer all arguments;)
	$(LI use $(D %%) for a $(D %) symbol.)
)
*/
mixin template Bind(string fmt)
{
	mixin("alias " ~ fmt.formatBind() ~ ";");
}

///
unittest
{
	import unstd.traits;

	mixin Bind!q{ CommonTypeToLong = CommonType!(long, %*) };
	static assert(is(CommonTypeToLong!int == long));

	mixin Bind!q{ isImplicitlyConvertibleToInt = isImplicitlyConvertible!(%0, int) };
	static assert(!isImplicitlyConvertibleToInt!long);

	mixin Bind!q{ isImplicitlyConvertibleFromInt = isImplicitlyConvertible!(int, %0) };
	static assert( isImplicitlyConvertibleFromInt!long);

	mixin Bind!q{ UnqualAll = MapTuple!(Unqual, %*) };
	static assert(is(UnqualAll!(const(int), immutable(bool[])) == TypeTuple!(int, immutable(bool)[])));
}

unittest
{
	void test(string fmt, size_t n, Args...)()
	{
		mixin Bind!("Res0 = " ~ fmt);
		static assert(Pack!(Res0!(Args[0 .. n])).equals!(Args[n .. $]));

		mixin Bind!(fmt ~ " Res");
		static assert(Pack!(Res!(Args[0 .. n])).equals!(Args[n .. $]));
	}

	alias Pack = PackedGenericTuple;
	static assert(Pack!(GenericTuple!(1, 2, int)).equals!(1, 2, int));
	test!(
		q{ GenericTuple!(1, 2, int) },
		1, 3,
		1, 2, int
	)();
	test!(
		q{ GenericTuple!(%0) },
		1, 3,
		3
	)();
	test!(
		q{ GenericTuple!(1, 2, int, %0) },
		1, 3,
		1, 2, int, 3
	)();
	test!(
		q{ GenericTuple!(1, 2, int, %*) },
		1, 3,
		1, 2, int, 3
	)();
	test!(
		q{ GenericTuple!(1, %0, 2, int, %0) },
		1, 3,
		1, 3, 2, int, 3
	)();
	test!(
		q{ GenericTuple!(1, %1, 2, %0, int, %0, %*,) },
		3, 3, char, 5,
		1, char, 2, 3, int, 3, 3, char, 5
	)();
}
