/**
Stuff for working with dynamic libraries.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module unstd.dynamiclib;


import std.array;
import std.conv;
import std.traits;

import unstd.c.string;

version(Windows)
{
	import core.sys.windows.windows;
	import std.windows.syserror;
}
else version(Posix)
{
	import core.sys.posix.dlfcn;
}
else
	static assert(0, "Not implemented");


version(UnstdDoc)
{
	/**
	Native dynamic library handle.

	It is $(D HMODULE) on $(I Windows) and $(D void*) on $(I Posix).
	*/
	alias DynamicLibHandle = void*;
	enum invalidDynamicLibHandle = null;
}
else version(Windows)
{
	alias DynamicLibHandle = HMODULE;
	enum invalidDynamicLibHandle = null;
}
else version(Posix)
{
	alias DynamicLibHandle = void*;
	enum invalidDynamicLibHandle = null;
}
else
	static assert(0, "Not implemented");


private T _enforce(T)(T value, lazy const(char)[] msg, string file = __FILE__, size_t line = __LINE__)
{
    if(value)
		return value;

	version(Windows)
		const errorStr = sysErrorString(GetLastError());
	else version(Posix)
		const errorStr = dlerror().toString();
	else
		static assert(0, "Not implemented");

	throw new Exception(text(msg, "\n", errorStr), file, line);
}

/**
This struct encapsulates functionality for working with dynamic libraries.

This struct is neither default constructable nor copyable.
Pass it by $(D ref) to functions or use $(STDREF typecons, RefCounted).
*/
struct DynamicLib
{
	// Fields
	// ----------------------------------------------------------------------------------------------------

	private
	{
		DynamicLibHandle _handle = invalidDynamicLibHandle;
		immutable bool _own;
	}

	// Constructors
	// ----------------------------------------------------------------------------------------------------

	@disable this();
	@disable this(this);

	/**
	Construct a $(D DynamicLib) from a manually obtained $(D DynamicLibHandle).

	Params:
	handle = A valid $(D DynamicLibHandle).
	own = Whether or not to close $(D handle) on destruction.

	Preconditions:
	$(D handle) is a valid $(D DynamicLibHandle).
	*/
	this(DynamicLibHandle handle, in bool own) @safe pure nothrow
	in { assert(handle != invalidDynamicLibHandle); }
	out { assert(associated); }
	body
	{
		_handle = handle;
		_own = own;
	}

	/**
	Construct a $(D DynamicLib) with the library name.

	Params:
	name = A name of the dynamic library to open.
	search = Whether system will search for dynamic library.
	If $(D false) $(D name) is expected to be a path for the dynamic library file.

	Preconditions:
	$(D !name.empty)

	Throws:
	$(D Exception) on opening error.

	Note:
	Be careful with $(D search = true) as it can lead to $(B security vulnerability) if used careless.
	*/
	this(in char[] name, in bool search = false) @trusted
	in { assert(!name.empty); }
	body
	{
		version(Windows)
		{
			import core.exception;
			import ascii = std.ascii;
			import std.path;
			import unstd.memory.allocation;
			import unstd.utf;

			const name2 = search || isAbsolute(name) ? name : buildPath(getcwd(), name);

			const needDot = !search && name2[$ - 1] != '.' &&
				(name2.length < 4 ||
				name2[$ - 4] != '.' ||
				ascii.toLower(name2[$ - 3]) != 'd' ||
				ascii.toLower(name2[$ - 2]) != 'l' ||
				ascii.toLower(name2[$ - 1]) != 'l');

			const wcharsCount = memoryAdd(maxLength!wchar(name2), 1 + needDot);
			if(!wcharsCount)
				onOutOfMemoryError();
			auto tmp = tempAlloc!wchar(wcharsCount);
			wchar* ptrW = &copyEncoded(name2, tmp.arr)[$ - 1];
			if(needDot)
				*++ptrW = '.';
			*++ptrW = '\0';
			auto handle = LoadLibraryW(tmp.ptr);
		}
		else version(Posix)
		{
			import std.path;
			import std.string;

			const name2 = search || name.indexOf('/') != -1 ? name : buildPath(getcwd(), name);

			const cName = name2.tempCString();
			auto handle = dlopen(cName, RTLD_NOW);
		}
		else
			static assert(0, "Not implemented");

		_enforce(handle != invalidDynamicLibHandle, "Failed to open: " ~ name);
		this(handle, true);
	}

	// Destructor
	// ----------------------------------------------------------------------------------------------------

	~this() @trusted
	{
		if(_own && associated)
			close();
	}

	// Properties
	// ----------------------------------------------------------------------------------------------------

	@property
	{
		/**
		Returns whether $(D this) is _associated with a dynamic library handle.
		It is asserted that no member functions are called for an unassociated
		$(D DynamicLib) struct.

		Examples:
		---
		assert(!DynamicLib.init.associated);
		auto h = DynamicLib.init.handle; // assertion failure
		---
		*/
		bool associated() const @safe pure nothrow
		{ return _handle != invalidDynamicLibHandle; }

		/**
		Returns whether handle of the associated dynamic library will be closed on destruction.
		*/
		bool ownHandle() const @safe pure nothrow
		in { assert(associated); }
		body { return _own; }

		/**
		Gets native _handle of the associated dynamic library.
		*/
		@property inout(DynamicLibHandle) handle() inout @safe pure nothrow
		in { assert(associated); }
		body { return _handle; }
	}

	// Operators
	// ----------------------------------------------------------------------------------------------------

	/**
	Returns the address of a symbol named $(D symbolName)
	or $(D null) if it is not found.

	Note:
	$(D null) return doesn't mean the symbol is not found as the address
	of a symbol may be $(D null).
	*/
	void* opBinaryRight(string op : "in")(in char[] symbolName) @trusted
	in { assert(associated); }
	body
	{
		version(Windows)
			return GetProcAddress(_handle, symbolName.tempCString());
		else version(Posix)
			return dlsym(_handle, symbolName.tempCString());
		else
			static assert(0, "Not implemented");
	}

	/**
	Returns the address of a symbol named $(D symbolName).

	Throws:
	$(D Exception) if the symbol is not found or has $(D null) address.
	*/
	void* opIndex(in char[] symbolName) @trusted
	in { assert(associated); }
	body
	{
		const cSymbolName = symbolName.tempCString();
		version(Windows)
		{
			void* res = GetProcAddress(_handle, cSymbolName);
		}
		else version(Posix)
		{
			dlerror(); // clear any existing error
			void* res = dlsym(_handle, cSymbolName);
		}
		else
			static assert(0, "Not implemented");
		return _enforce(res, "Symbol not found: " ~ symbolName);
	}

	// Functions
	// ----------------------------------------------------------------------------------------------------

	/**
	Calls the function named $(D functionName) from this dynamic library.

	Throws:
	$(D Exception) if the function is not found or has $(D null) address.

	Examples:
	---
	extern(C) alias F = int function(int) nothrow;
	const int res = dynLib.call!F("f", 5);
	---
	*/
	auto ref call(T)(in char[] functionName, auto ref ParameterTypeTuple!T params) @system
	in { assert(associated); }
	body
	{
		return (cast(T) opIndex(functionName))(params);
	}

	/**
	Returns the reference to the variable named $(D varName) from this dynamic library.

	Throws:
	$(D Exception) if the variable is not found or has $(D null) address.

	Examples:
	---
	dynLib.var!int("i") = 3;
	int* j = &dynLib.var!int("j")
	---
	*/
	ref T var(T)(in char[] varName) @system
	in { assert(associated); }
	body
	{
		return *(cast(T*) opIndex(varName));
	}

	/**
	Closes native handle and makes the struct unassociated.

	Throws:
	$(D Exception) on closing error.
	*/
	void close() @trusted
	in { assert(associated); }
	out { assert(!associated); }
	body
	{
		version(Windows)
			const res = FreeLibrary(_handle) != 0;
		else version(Posix)
			const res = dlclose(_handle) == 0;
		else
			static assert(0, "Not implemented");
		_enforce(res, "Failed to close dynamic library.");
		_handle = invalidDynamicLibHandle;
	}
}

/**
Tries to set $(D sym) to point to a symbol named $(D symbolName) in dynamic library $(D lib).

Examples:
---
extern(C) alias F = void function(int) nothrow;
F f;
if(dynLib.tryBind!f())
	f(5);
---
*/
bool tryBind(alias sym)(auto ref DynamicLib lib, in char[] symbolName = sym.stringof) @system
{
	sym = cast(typeof(sym)) (symbolName in lib);
	return sym !is null;
}

/**
Sets $(D sym) to point to a symbol named $(D symbolName) in dynamic library $(D lib).

Throws:
$(D Exception) if the symbol is not found or has $(D null) address.

Examples:
---
extern(C) alias F = void function(int) nothrow;
F f;
dynLib.bind!f();
f(5);
---
*/
void bind(alias sym)(auto ref DynamicLib lib, in char[] symbolName = sym.stringof) @system
if(!is(sym))
{
	sym = cast(typeof(sym)) lib[symbolName];
}

/**
Returns an instance of struct $(D T) with all fields pointing to corresponding
symbols in dynamic library $(D lib).

Throws:
$(D Exception) if any corresponding symbol is not found or has $(D null) address.

Examples:
---
version(Windows):

struct Kernel32
{
extern(Windows) nothrow:
	alias DWORD = uint;
	DWORD function() GetVersion, GetTickCount;
}
const kernel32 = DynamicLib("Kernel32.dll", true).bind!Kernel32();

import std.stdio;
writefln("GetVersion: %X", kernel32.GetVersion());
writefln("GetTickCount: %s", kernel32.GetTickCount());
---
*/
T bind(T)(auto ref DynamicLib lib) @system
if(is(T == struct))
{
	T res;
	foreach(i, ref field; res.tupleof)
		field = cast(typeof(field)) lib[__traits(identifier, res.tupleof[i])];
	return res;
}

version(Windows) unittest
{
	{
		static assert(!DynamicLib.init.associated);
		auto dl = DynamicLib("Gdi32.dll", true);
		assert(dl.associated && dl.ownHandle);
		assert("GetNearestColor" in dl);
		assert(dl["GetNearestColor"]);
		assert("GetNearestColorXXX" !in dl);
		dl.close();
		assert(!dl.associated);
	}
	{
		auto dl = DynamicLib("Kernel32.dll", true);

		enum name = "GetVersion";
		const val = GetVersion();
		alias T = typeof(&GetVersion);

		auto f = cast(T) dl[name];
		assert(f && f() == val);
		assert(f == cast(void*) GetProcAddress(GetModuleHandleA("Kernel32"), name));
		assert(dl.call!T(name) == val);
		assert(cast(void*) &dl.var!int(name) == f);

		T GetVersion;
		assert(dl.tryBind!GetVersion() && GetVersion == f);
		GetVersion = null;
		dl.bind!GetVersion();
		assert(GetVersion == f);

		struct Kernel32 { T GetVersion; static void f(); }
		assert(dl.bind!Kernel32().GetVersion == f);

		// Windows loads library again here:
		assert(DynamicLib("Kernel32.dll.", true)[name] != f);
	}
}
