﻿Unstandard
===================================
*Unstandard* is a library for general purpose usage aimed to be
an addition to the [D](http://dlang.org) standard runtime library
[Phobos](http://dlang.org/phobos/).

Read [documentation](http://denis-sh.bitbucket.org/unstandard/) for more information.

License
-----------------------------------
The project is licensed under the terms of the [Boost Software License, Version 1.0](http://boost.org/LICENSE_1_0.txt).
